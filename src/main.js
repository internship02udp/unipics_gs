// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/main.css";

// Global Layout
import MainLayout from "./layouts/MainLayout.vue";

// Global Component
import Banner from "./components/global/Banner.vue";
import Section from "./components/global/Section.vue";
import ImageBoxCarousel from "./components/global/ImageBoxCarousel.vue";
import TestimonialCarousel from "./components/global/TersimonialCarousel.vue";

export default function(Vue, { router, head, isClient }) {
  // Font
  head.link.push({
    rel: "stylesheet",
    href:
      "https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap"
  });

  // Vue.use(BootstrapVue);

  // Set default layout as a global component
  Vue.component("Layout", MainLayout);
  Vue.component("Banner", Banner);
  Vue.component("Section", Section);
  Vue.component("ImageBoxCarousel", ImageBoxCarousel);
  Vue.component("TestimonialCarousel", TestimonialCarousel);
}
